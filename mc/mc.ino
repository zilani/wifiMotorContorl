#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiManager.h> 
#include <ArduinoJson.h>

#define DEBUG true

#define WIFI_TIMEOUT  5000
#define AP_SSID       "Motor_Device"
#define AP_PASSWORD   "12345678"
#define DEVICE_ID     String("12345")
#define PORTAL_TIMEOUT  180


#define newChangesCheck   String("http://abdulhalim.pythonanywhere.com/motorcontrol/default/changes?deviceid=" + DEVICE_ID)
#define dataLink          String("http://abdulhalim.pythonanywhere.com/motorcontrol/default/get_instruction?deviceid=" + DEVICE_ID)
#define acknowledge       String("http://abdulhalim.pythonanywhere.com/motorcontrol/default/data_received?deviceid="+ DEVICE_ID)


#define M1a  12
#define M1b  13
#define M1s  14
#define PWM_FREQ  7

struct motor{
  bool onOffStatus;
  byte rSpeed;
  bool mDirection;
  };

motor motor1;
  
WiFiManager wifiManager;


void setup() {

    pinMode(M1a,OUTPUT);
    pinMode(M1b,OUTPUT);
    pinMode(M1s,OUTPUT);
    analogWriteFreq(PWM_FREQ);
#ifdef DEBUG 
    Serial.begin(115200);
    Serial.println();
    Serial.println();
#endif


  //wifiManager.resetSettings();
  wifiManager.setTimeout(WIFI_TIMEOUT);
  wifiManager.setConfigPortalTimeout(PORTAL_TIMEOUT);
  wifiManager.autoConnect(AP_SSID,AP_PASSWORD);

  if(WiFi.status()==WL_CONNECTED) {
      getData();
      motor1Control(motor1.onOffStatus,motor1.mDirection,motor1.rSpeed);
      dataIsRecieved();
    }
   /*
    motor1Control(1,1,50);
    delay(2000);
    motor1Control(1,0,150);
    delay(2000);
    motor1Control(1,1,250);
    delay(2000);
    motor1Control(1,0,255);
    delay(2000);
*/

}

void loop() {

  if(WiFi.status()==WL_CONNECTED) {
    if(isDataAvailable()){
      getData();
      motor1Control(motor1.onOffStatus,motor1.mDirection,motor1.rSpeed);
      dataIsRecieved();
      }
    isMotorOn();
  }
  
}





void getData(){
  
   HTTPClient http;
   http.begin(dataLink);
   int httpCode = http.GET();
   
   String payload;
   
   if(httpCode > 0) {
        if(httpCode == HTTP_CODE_OK) {
                payload = http.getString();
                
            }
        }
   
   http.end();

#ifdef DEBUG 
   Serial.println(dataLink);
   Serial.println(httpCode);
   Serial.println(payload);
#endif


   StaticJsonBuffer<1000>jsonBuffer;
   JsonObject& root = jsonBuffer.parseObject(payload);

   if(!root.success()){
    #ifdef DEBUG 
    Serial.println(F("josn parsing failed"));
    #endif
      return;
    } 


   if(root["device_info"][0]["onoff"]== "T"){
    motor1.onOffStatus=true;
   }
   else motor1.onOffStatus=false;
   
   if(root["device_info"][0]["direction"]=="Forward"){
    motor1.mDirection=true;
   }
   else motor1.mDirection=false;
   
   motor1.rSpeed = (byte)root["device_info"][0]["rotation"];

#ifdef DEBUG 
   Serial.println(motor1.onOffStatus);
   Serial.println(motor1.mDirection);
   Serial.println(motor1.rSpeed);
#endif  
  }




  

bool isDataAvailable(){
  HTTPClient http;
  http.begin(newChangesCheck);
  int httpCode = http.GET();
  String payload;
   if(httpCode > 0) {
        if(httpCode == HTTP_CODE_OK) {
                payload = http.getString();
            }
        }
   http.end();
    
#ifdef DEBUG 
   Serial.println(newChangesCheck);
   Serial.println(httpCode);
   Serial.println(payload);
#endif 


   StaticJsonBuffer<100>jsonBuffer;
   JsonObject& root = jsonBuffer.parseObject(payload);
   
   if((bool)root["change"])return true;
   else return false;
      
  }


void dataIsRecieved(){
   HTTPClient http;
   http.begin(acknowledge);
   int httpCode = http.GET();

   #ifdef DEBUG 
   Serial.println(acknowledge);
   Serial.println(httpCode);
   #endif
   
   http.end();
        
        }



void motor1Control(bool mStatus, bool direct, byte spd){
  if(mStatus){
    if(direct){
      digitalWrite(M1a,LOW);
      digitalWrite(M1b,HIGH);
      }
    else{
      digitalWrite(M1a,HIGH);
      digitalWrite(M1b,LOW);
      }
     analogWrite(M1s,spd);
    }

    else{
      digitalWrite(M1a,HIGH);
      digitalWrite(M1b,HIGH);
      }
   
  }


bool isMotorOn(){
  int turn=100;
  int prev=analogRead(A0);
  int diff=0;
  for(int i=0;i<turn;i++){
    int value=analogRead(A0);
    diff+=abs(value-prev);
    prev=value;
    delay(10);
    }
   diff=diff/turn;
   //if(diff==0)return false;
   //else return true;

   StaticJsonBuffer<200> jsonBuffer;
   JsonObject& root = jsonBuffer.createObject();
   JsonArray& notification = root.createNestedArray("notification");
   JsonObject& notific=notification.createNestedObject();
   notific["device_id"]="ss";
   notific["msg"]="ss";
   notific["time"]="ss";
   root.prettyPrintTo(Serial);
   
  }
